#ifndef ABB_HPP
#define ABB_HPP

#include <aikido/constraint/NonColliding.hpp>
#include <aikido/constraint/TSR.hpp>
#include <aikido/statespace/dart/MetaSkeletonStateSpace.hpp>
#include <aikido/trajectory/Interpolated.hpp>
#include <aikido/trajectory/Trajectory.hpp>
#include <dart/dart.hpp>
#include <memory>

namespace abb {

    class Abb final
    {
    public:
      const dart::common::Uri abbUri{
          "package://ai_environment/urdf/robot.urdf"};
          //"package://ai_environment/urdf/irb140.urdf"};
          //"package://herb_description/robots/herb.urdf"};

      /// Construct the ABB metaskeleton
      Abb();

      /// Get the robot skeleton
      dart::dynamics::SkeletonPtr getSkeleton() const;

      /// Get the right arm
      dart::dynamics::ChainPtr getArm() const;

      /// Get the right end-effector
      dart::dynamics::BodyNodePtr getEndEffector() const;

      std::vector<dart::dynamics::BodyNodePtr> getGripperNodes();

      /// Get home configuration
      Eigen::VectorXd getHomeConfiguration() const;

      /// Compute velocity limits from the MetaSkeleton
      Eigen::VectorXd getVelocityLimits(
          dart::dynamics::MetaSkeleton& _metaSkeleton) const;

      /// Compute acceleration limits from the MetaSkeleton
      Eigen::VectorXd getAccelerationLimits(
          dart::dynamics::MetaSkeleton& _metaSkeleton) const;

      /// Set the configuration of the metaskeleton defined in the given statespace
      /// \param _space The StateSpace for the metaskeleton
      /// \param _configuration The configuration to set
      void setConfiguration(
          aikido::statespace::dart::MetaSkeletonStateSpacePtr _space,
          const Eigen::VectorXd& _configuration);

    protected:
      aikido::constraint::NonCollidingPtr getSelfCollisionConstraint(
          aikido::statespace::dart::MetaSkeletonStateSpacePtr _space) const;

    private:
      double mCollisionResolution;
      dart::dynamics::SkeletonPtr mRobot;
      dart::dynamics::ChainPtr mArm;
      dart::dynamics::BodyNodePtr mEndEffector;
      dart::dynamics::InverseKinematicsPtr mIk;
    };

}

#endif  // ABB_HPP
