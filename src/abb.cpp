#include <ai_environment/abb.hpp>

#include <random>
#include <stdexcept>
#include <string>
#include <chrono>

#include <Eigen/Core>
#include <aikido/constraint/JointStateSpaceHelpers.hpp>
#include <aikido/constraint/InverseKinematicsSampleable.hpp>
#include <aikido/constraint/FiniteSampleable.hpp>
#include <aikido/constraint/CyclicSampleable.hpp>
#include <aikido/constraint/Testable.hpp>
#include <aikido/constraint/TestableIntersection.hpp>
#include <aikido/distance/defaults.hpp>
#include <aikido/planner/ompl/CRRTConnect.hpp>
#include <aikido/planner/ompl/Planner.hpp>
#include <aikido/statespace/GeodesicInterpolator.hpp>
#include <aikido/statespace/dart/MetaSkeletonStateSpace.hpp>
#include <aikido/util/CatkinResourceRetriever.hpp>
#include <aikido/util/RNG.hpp>
#include <dart/utils/urdf/DartLoader.hpp>
#include <ompl/geometric/planners/rrt/RRTConnect.h>

namespace abb {

    using aikido::statespace::dart::MetaSkeletonStateSpacePtr;
    using aikido::trajectory::InterpolatedPtr;
    using aikido::constraint::NonCollidingPtr;
    using dart::common::make_unique;
    using dart::dynamics::BodyNodePtr;
    using dart::dynamics::ChainPtr;
    using dart::dynamics::MetaSkeleton;
    using dart::dynamics::SkeletonPtr;

    Abb::Abb()
    {
        using dart::dynamics::InverseKinematics;
        using dart::dynamics::Chain;
        dart::utils::DartLoader urdfLoader;

        auto resourceRetriever =
          std::make_shared<aikido::util::CatkinResourceRetriever>();
        mRobot = urdfLoader.parseSkeleton(abbUri, resourceRetriever);

        if (!mRobot) {
            throw std::runtime_error("unable to load ABB model");
        }

        auto nodes = mRobot->getBodyNodes();

        auto armBase = mRobot->getBodyNode("base_link");
        mEndEffector = mRobot->getBodyNode("link_6");

        mArm = Chain::create(armBase, mEndEffector, "arm");

        mIk = InverseKinematics::create(mEndEffector);
        mIk->setDofs(mArm->getDofs());

        std::cout << "Robot setup complete" << std::endl;
    }

    // ==============================================================================

    SkeletonPtr Abb::getSkeleton() const { return mRobot; }

    std::vector<dart::dynamics::BodyNodePtr> Abb::getGripperNodes()
    {
        std::vector<dart::dynamics::BodyNodePtr> gripperNodes;
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_adapter_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_base_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_left_knuckle_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_right_knuckle_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_right_knuckle_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_left_finger_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_right_finger_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_left_inner_knuckle_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_right_inner_knuckle_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_left_finger_tip_link"));
        gripperNodes.push_back(mRobot->getBodyNode("robotiq_85_right_finger_tip_link"));

        return gripperNodes;

    } 


    // ==============================================================================

    ChainPtr Abb::getArm() const { return mArm; }

    // ==============================================================================

    BodyNodePtr Abb::getEndEffector() const { return mEndEffector; }

    // ==============================================================================

    Eigen::VectorXd Abb::getVelocityLimits(MetaSkeleton& _metaSkeleton) const
    {
      Eigen::VectorXd velocityLimits(_metaSkeleton.getNumDofs());

      for (size_t i = 0; i < velocityLimits.size(); ++i) {
        velocityLimits[i] = std::min(-_metaSkeleton.getVelocityLowerLimit(i),
                                     +_metaSkeleton.getVelocityUpperLimit(i));
        // TODO: Warn if assymmetric.
      }

      return velocityLimits;
    }

    // ==============================================================================

    Eigen::VectorXd Abb::getAccelerationLimits(MetaSkeleton& _metaSkeleton) const
    {
      Eigen::VectorXd accelerationLimits(_metaSkeleton.getNumDofs());

      for (size_t i = 0; i < accelerationLimits.size(); ++i) {
        accelerationLimits[i] =
            std::min(2.0,
                     std::min(-_metaSkeleton.getAccelerationLowerLimit(i),
                              +_metaSkeleton.getAccelerationUpperLimit(i)));

        // TODO: Warn if asymmetric.
      }

      return accelerationLimits;
    }

    // ==============================================================================

    Eigen::VectorXd Abb::getHomeConfiguration() const
    {

        // Set start configuration
        auto nof_dimensions = mArm->getNumJoints();
        Eigen::VectorXd homeConfiguration(nof_dimensions);
        for (size_t i = 0; i < nof_dimensions; ++i){
            double pos = mArm->getJoint(i)->getInitialPosition(0);
            homeConfiguration(i) = pos;
        }

        return homeConfiguration;
    }

    // ==============================================================================

    void Abb::setConfiguration(MetaSkeletonStateSpacePtr _space,
                                const Eigen::VectorXd& _configuration)
    {
      auto state = _space->createState();
      _space->convertPositionsToState(_configuration, state);
      _space->setState(state);
    }

}
